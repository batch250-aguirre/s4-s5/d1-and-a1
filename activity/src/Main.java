import com.zuitt.example.*;


public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("Zuitt Coding Bootcamp ", "+639152468596", "my home is in Quezon City");
        Contact contact2 = new Contact("Rupert Ramos", "+639162148573", "my home is in Caloocan City");
        Contact contact3 = new Contact("Michael Luis Aguirre", "+639123456798", "my home is in Antipolo City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        phonebook.setContacts(contact3);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                printInfo(contact);
            }
        }
    }

    public static void printInfo(Contact contact) {
        System.out.println("-----------------------");
        System.out.println(contact.getName());
        System.out.println("-----------------------");

        if (contact.getContactNumber() != null) {
            System.out.println(contact.getName() + " has the following registered number:");
            System.out.println(contact.getContactNumber());
        } else {
            System.out.println(contact.getName() + " has no registered number.");
        }

        if (contact.getAddress() != null) {
            System.out.println(contact.getName() + " has the following registered address:");
            System.out.println(contact.getAddress());
        } else {
            System.out.println(contact.getName() + " has no registered address.");
        }
    }
}